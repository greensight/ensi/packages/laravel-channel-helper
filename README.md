# Laravel Log Channel Helper

Пакет для дублирования логов из daily и single каналов в stdout.

## Установка

1. Добавьте в composer.json в repositories 

```
repositories: [
    {
        "type": "vcs",
        "url": "https://gitlab.com/greensight/ensi/packages/laravel-channel-helper.git"
    }
],

```

2. `composer require ensi/laravel-channel-helper`
3. Оберните описание каналов логирования в `config/logging.php` в вызов `ChannelHelper::addStdoutStacks()`
4. Добавляйте новые каналы логирования используя вспомогательные функции

Пример:

```
return ChannelHelper::addStdoutStacks([
    'default' => env('LOG_CHANNEL', 'stack'),
    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'name' => 'default',
            'channels' => ChannelHelper::getNamesForStack('daily'),
            'ignore_exceptions' => false,
        ],

        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
        ],

        'daily' => ChannelHelper::makeDailyChannel('laravel.log'),
        'checkout' => ChannelHelper::makeDailyChannel('checkout/laravel.log', 3)
    ],
]);

```

## Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).
