<?php

namespace Ensi\LaravelChannelHelper;

use Monolog\Handler\StreamHandler;

class ChannelHelper
{
    private const STDOUT_CHANNEL_NAME = 'stdout';

    public static function addStdoutStacks(array $config): array
    {
        $newChannels = [
            self::STDOUT_CHANNEL_NAME => self::makeStdoutChannel(self::STDOUT_CHANNEL_NAME)
        ];

        foreach ($config['channels'] as $key => $oldChannel) {
            $driver = $oldChannel['driver'] ?? null;

            if ($driver == 'daily' || $driver == 'single') {
                $fileKey = "{$key}:file";
                $oldChannel['name'] = $key;
                $newChannels[$fileKey] = $oldChannel;
                $newChannels[$key] = self::makeStackChannel($key, [self::STDOUT_CHANNEL_NAME, $fileKey]);
            } else {
                $newChannels[$key] = $oldChannel;
            }
        }
        $config['channels'] = $newChannels;

        return $config;
    }

    public static function getNamesForStack(string $name): array
    {
        return [self::STDOUT_CHANNEL_NAME, "{$name}:file"];
    }

    public static function makeStdoutChannel(string $name): array
    {
        return [
            'name' => $name,
            'driver' => 'monolog',
            'level' => env('LOG_LEVEL', 'debug'),
            'handler' => StreamHandler::class,
            'with' => [
                'stream' => 'php://stdout',
            ],
        ];
    }

    public static function makeStackChannel(string $name, array $channels): array
    {
        return [
            'driver' => 'stack',
            'name' => $name,
            'channels' => $channels,
            'ignore_exceptions' => false,
        ];
    }

    public static function makeDailyChannel(string $path, int $ttlDays = 14): array
    {
        /** @noinspection PhpUndefinedFunctionInspection */
        return [
            'driver' => 'daily',
            'path' => storage_path('logs/' . $path),
            'level' => env('LOG_LEVEL', 'debug'),
            'days' => $ttlDays,
        ];
    }
}
